const mongoose = require("mongoose");
const timestamp = require("mongoose-timestamp");

const scheme = new mongoose.Schema( {
    email: {
        type: String,
        required: true
    },
    password:{
        type:String,
        required:true
    },
    profile: {
        name: {
            type: String,
            required: true,
        },
        address: {
            type: String,
            required: true
        },
        city_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "city"
        },
        hobbies: {
            type: [String],
            require:true,
        },
    },
    last_login:Date
}).plugin(timestamp)

const user = mongoose.model("user",scheme)

module.exports = user;