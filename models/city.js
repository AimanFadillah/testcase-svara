const mongoose = require("mongoose");
const timestamp = require("mongoose-timestamp");

const schema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    }
}).plugin(timestamp);

const city = mongoose.model("city", schema);

module.exports = city;