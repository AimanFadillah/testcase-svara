const app = require("express")();
const cookieParser = require("cookie-parser");
const fileUpload = require("express-fileupload");
const cors = require("cors");
const route = require("./api");
const dotenv = require("dotenv");

dotenv.config();
require("./config/db")

app.use(cors())
app.use(cookieParser());
app.use(fileUpload());
app.use("/api",route);

app.get("*", (req, res) => res.status(404).send("Not Found"));

app.listen(5000, () => console.log("http://localhost:5000"))

