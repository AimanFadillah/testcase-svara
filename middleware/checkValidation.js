const { validationResult } = require("express-validator");

module.exports = function checkValidation (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(403).json(errors.array());
    return next();
}