const city = require("../models/city");

const cityController = {
    get:async function (req,res) {  
        const data = await city.find({});
        return res.json(data)
    }
}

module.exports = cityController;