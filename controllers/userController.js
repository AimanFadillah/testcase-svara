const { body, param, validationResult } = require("express-validator");
const city = require("../models/city");
const User = require("../models/user");
const checkValidation = require("../middleware/checkValidation");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const userController = {

    get: async function (req, res) {
        const page = req.query.page || 1;
        const perpage = 20;
        const data = await User.find({}, "-password")
            .populate("profile.city_id")
            .skip((page <= 0 ? 1 : page - 1) * perpage)
            .limit(perpage);
        return res.status(200).json(data);
    },

    store: [
        body("email").notEmpty().withMessage("Email is required")
            .isString().withMessage("Email must be string")
            .isEmail().withMessage("Email is invalid")
            .custom(async email => {
                const checkUser = await User.findOne({ email });
                if (checkUser) {
                    throw new Error("Email not unique");
                }
                return true
            }),

        body("password").notEmpty().withMessage("Password is required")
            .isString().withMessage("Password must be string"),

        body("confirm_password").notEmpty().withMessage("Confirm Password is required")
            .isString().withMessage("Confirm Password must be string")
            .custom((confirmPassword, { req }) => {
                if (confirmPassword !== req.body.password) {
                    throw new Error("Password and Confirm password do not match")
                }
                return true
            }),

        body("name").notEmpty().withMessage("Name is required")
            .isString().withMessage("Name must be string"),

        body("city_id").notEmpty().withMessage("City is required")
            .custom(async id => {
                const check = await city.findById(id);
                if (!check) {
                    throw new Error("City does not exist")
                }
                return true
            }),

        body("address").notEmpty().withMessage("Address is required")
            .isString().withMessage("Address must be string"),

        body("hobbies").notEmpty().withMessage("hobbies is required")
            .isArray().withMessage("hobbies must be array"),

        checkValidation,
        async function (req, res) {
            let { email, password, name, address, city_id, hobbies } = req.body;
            password = bcrypt.hashSync(password, 10);
            const data = {
                email,
                password,
                profile: {
                    name,
                    address,
                    city_id,
                    hobbies,
                },
                last_login: ""
            }

            new User(data).save();
            return res.status(201).json({ msg: "User successfully added" });
        }
    ],

    show: [
        param("id").custom(async (id, { req }) => {
            const checkUser = await User.findById(id).populate("profile.city_id");
            if (!checkUser) {
                throw new Error("User not found")
            }
            req.user = checkUser;
            return true
        }),
        function (req, res, next) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) return res.status(404).json({ msg: "Not Found" });
            return next();
        },
        async function (req, res) {
            return res.status(200).json(req.user);
        }
    ],

    update: [
        body("current_password").notEmpty().withMessage("Current Password is required")
            .isString().withMessage("Current Password must be string")
            .custom(async (password, { req }) => {
                const user = await User.findOne(req.user.id);
                if (!bcrypt.compareSync(password, user.password)) {
                    throw new Error("Incorrect Password");
                }
                return true
            }),

        body("new_password").notEmpty().withMessage("New Password is required")
            .isString().withMessage("New Password must be string"),

        body("confirm_password").notEmpty().withMessage("Confirm Password is required")
            .isString().withMessage("Confirm Password must be string")
            .custom((confirmPassword, { req }) => {
                if (confirmPassword !== req.body.new_password) {
                    throw new Error("Password and Confirm password do not match")
                }
                return true
            }),
        checkValidation,
        async function (req, res) {
            await User.findByIdAndUpdate(req.user._id, {
                password: bcrypt.hashSync(req.body.new_password, 10)
            });
            return res.status(201).json({ msg: "User successfully updated" })
        }
    ],

    destroy: [
        param("id").custom(async (id, { req }) => {
            const checkUser = await User.findByIdAndDelete(id);
            if (!checkUser) {
                throw new Error("User not found")
            }
            req.user = checkUser;
            return true
        }),
        function (req, res, next) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) return res.status(404).json({ msg: "Not Found" });
            return next();
        },
        async function (req, res) {
            return res.status(200).json({ msg: "User successfully deleted" });
        }
    ],

    login: [
        body("email").notEmpty().withMessage("Email is required")
            .isString().withMessage("Email must be string")
            .isEmail().withMessage("Email is invalid")
            .custom(async (email, { req }) => {
                const checkUser = await User.findOne({ email });

                if (!checkUser) {
                    throw new Error("Incorrect Email or Password");
                }
                req.body.user = checkUser.toJSON();
                return true
            }),

        body("password").notEmpty().withMessage("Password is required")
            .isString().withMessage("Password must be string")
            .custom((password, { req }) => {
                if (!req.body.user || !bcrypt.compareSync(password, req.body.user.password)) {
                    throw new Error("Incorrect Email or Password");
                }
                return true
            }),

        function (req, res, next) {
            const errors = validationResult(req);
            if (!errors.isEmpty()) return res.status(401).json(errors.array());
            return next();
        },
        async function (req, res) {
            const { user } = req.body;
            await User.findByIdAndUpdate(user._id, { last_login: (new Date).toISOString() })
            delete user.password
            const token = jwt.sign(user, process.env.JWT_TOKEN, { expiresIn: 1000 * 60 * 60 * 3 })
            res.cookie("login", token, { httpOnly: true, maxAge: 1000 * 60 * 60 * 3 })
            return res.status(200).json("Login Success");
        }
    ]
}

module.exports = userController;