const express = require("express");
const cityController = require("./controllers/cityController");
const userController = require("./controllers/userController");
const loginMiddleware = require("./middleware/loginMiddleware");
const route = express.Router();

route.get("/", (req, res) => res.status(200).json("Welcome"));
route.get("/city", cityController.get);
route.post("/register", userController.store);
route.post("/login", userController.login);
route.delete("/user/:id", userController.destroy)

route.get("/user",loginMiddleware,userController.get)
route.put("/user",loginMiddleware,userController.update)
route.get("/user/:id",loginMiddleware,userController.show)

module.exports = route

